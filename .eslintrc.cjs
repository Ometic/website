/** @type {import("eslint").Linter.Config} */
module.exports = {
    extends: ["plugin:astro/recommended"],
    parser: "@babel/eslint-parser",
    parserOptions: {
      requireConfigFile: false
    },
    overrides: [
      {
        files: ["*.astro"],
        parser: "astro-eslint-parser",
        parserOptions: {
          parser: "@typescript-eslint/parser",
          extraFileExtensions: ["astro"]
        }
      }
  ]
}
