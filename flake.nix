{
  description = "My personal website built with astro";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = [ "x86_64-linux" ];

      perSystem = { config, pkgs, ... }: {
        devShells.default = pkgs.mkShell {
          name = "Website";
          packages = with pkgs; [
            nodejs_18
            nodePackages_latest.pnpm
            nodePackages_latest.typescript-language-server
            nodePackages_latest.eslint_d
            nodePackages_latest."@astrojs/language-server"
            prettierd
            cz-cli
            commitizen
          ];
        };
      };
    };
}
